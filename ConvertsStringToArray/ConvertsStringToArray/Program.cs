﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertsStringToArray
{
    class Program
    {
        static void Main(string[] args)
        {
            //Store of variables
            string one = "red";
            string two = "blue";
            string three = "orange";
            string four = "black";
            string five = "white";
            string name;

            //Explanation to the user
            Console.WriteLine("Hi there, what's your name user?");
            name = Console.ReadLine();
            Console.WriteLine("Nice to meet you {0}. This program has a series of different strings that it's going to amalgamate into an array.", name);
            Console.WriteLine("Let's see the strings we have.");
            Console.WriteLine();
            Console.WriteLine(one);
            Console.WriteLine(two);
            Console.WriteLine(three);
            Console.WriteLine(four);
            Console.WriteLine(five);
            Console.WriteLine();
            Console.WriteLine("Alright, now lets convert these into an array.");
            //Creates a string that assimilates the values of variables one, two, three, four, and five
            string[] colours = new string[5] { one, two, three, four, five };
            Console.WriteLine("and then lets print the array.");
            Console.WriteLine();
            foreach (string i in colours)
            {
                Console.WriteLine(i);
            }
        }
    }
}
